package com.bj.com_manager_api.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ComIsGood {

    GOOD("정상"),
    BREAK_DOWN("고장"),
    CHECKING("점검중"),
    NEED_CHECK("점검필수"),
    UNDER_REPAIR("수리중");


    private final String name;

}