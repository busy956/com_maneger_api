package com.bj.com_manager_api.controller;

import com.bj.com_manager_api.model.ComItem;
import com.bj.com_manager_api.model.ComRequest;
import com.bj.com_manager_api.model.ComResponse;
import com.bj.com_manager_api.service.ComputerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/computer")
public class ComController {
    private final ComputerService computerService;

    @PostMapping("/status")
    public String setComputer(@RequestBody ComRequest Request) {
        computerService.setComputer(Request);

        return "OK";
    }

    @GetMapping("/all")
    public List<ComItem> getComputers() {
        return computerService.getComputers();
    }

    @GetMapping("/detail/{id}")
    public ComResponse getComputer(@PathVariable long id) {
        return computerService.getComputer(id);
    }
}
