package com.bj.com_manager_api.model;

import com.bj.com_manager_api.enums.ComIsGood;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class ComResponse {
    private Long id;
    private Short seatNumber;
    private LocalDate upgradeDate;
    private String isUp;
    private String upContent;
    private String isCheck;
    private String checkContent;
    private String comIsGood;
    private String etcMemo;
}
