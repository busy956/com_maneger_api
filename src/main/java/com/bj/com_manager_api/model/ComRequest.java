package com.bj.com_manager_api.model;

import com.bj.com_manager_api.enums.ComIsGood;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class ComRequest {

    private Short seatNumber;

    private Boolean isUp;

    private String upContent;

    private Boolean isCheck;

    private String checkContent;

    @Enumerated(value = EnumType.STRING)
    private ComIsGood comIsGood;

    private String etcMemo;
}
