package com.bj.com_manager_api.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class ComItem {
    private Long id;
    private Short seatNumber;
    private LocalDate upgradeDate;
    private Boolean IsUp;
    private String upContent;
    private Boolean IsCheck;
    private String checkContent;
    private String comIsGood;
}
