package com.bj.com_manager_api.service;

import com.bj.com_manager_api.entity.Computer;
import com.bj.com_manager_api.model.ComItem;
import com.bj.com_manager_api.model.ComRequest;
import com.bj.com_manager_api.model.ComResponse;
import com.bj.com_manager_api.repository.ComRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ComputerService {
    private final ComRepository comRepository;

    public void setComputer(ComRequest request) {
        Computer addData = new Computer();
        addData.setSeatNumber(request.getSeatNumber());
        addData.setUpgradeDate(LocalDate.now());
        addData.setUpContent(request.getUpContent());
        addData.setCheckContent(request.getCheckContent());
        addData.setIsUp(request.getIsUp());
        addData.setIsCheck(request.getIsCheck());
        addData.setComIsGood(request.getComIsGood());
        addData.setEtcMemo(request.getEtcMemo());

        comRepository.save(addData);
    }

    public List<ComItem> getComputers() {
        List<Computer> originList = comRepository.findAll();

        List<ComItem> result = new LinkedList<>();

        for (Computer computer : originList) {
            ComItem addItem = new ComItem();
            addItem.setId(computer.getId());
            addItem.setSeatNumber(computer.getSeatNumber());
            addItem.setUpgradeDate(computer.getUpgradeDate());
            addItem.setIsUp(computer.getIsUp());
            addItem.setUpContent(computer.getUpContent());
            addItem.setIsCheck(computer.getIsCheck());
            addItem.setCheckContent(computer.getCheckContent());
            addItem.setComIsGood(computer.getComIsGood().getName());

            result.add(addItem);
        }

        return result;
    }

    public ComResponse getComputer(long id) {
        Computer originData = comRepository.findById(id).orElseThrow();

        ComResponse response = new ComResponse();
        response.setId(originData.getId());
        response.setSeatNumber(originData.getSeatNumber());
        response.setUpgradeDate(originData.getUpgradeDate());
        response.setIsUp(originData.getIsUp() ? "예" : "아니오");
        response.setUpContent(originData.getUpContent());
        response.setIsCheck(originData.getIsCheck() ? "예" : "아니오");
        response.setCheckContent(originData.getCheckContent());
        response.setComIsGood(originData.getComIsGood().getName());
        response.setEtcMemo(originData.getEtcMemo());

        return response;
    }
}
