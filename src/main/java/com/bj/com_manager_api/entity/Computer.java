package com.bj.com_manager_api.entity;

import com.bj.com_manager_api.enums.ComIsGood;
import jakarta.persistence.*;
import lombok.Generated;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Computer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private Short seatNumber;

    @Column(nullable = false)
    private LocalDate upgradeDate;

    @Column(nullable = false)
    private Boolean isUp;

    @Column(length = 40)
    private String upContent;

    @Column(nullable = false)
    private Boolean isCheck;

    @Column(length = 40)
    private String checkContent;

    @Enumerated(value = EnumType.STRING)
    private ComIsGood comIsGood;

    @Column(columnDefinition = "TEXT")
    private String etcMemo;

}
