package com.bj.com_manager_api.repository;

import com.bj.com_manager_api.entity.Computer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ComRepository extends JpaRepository<Computer, Long> {
}
